#!/usr/bin/env python
#
# Copyright (c) 2016
# Author: Victor Arribas <v.arribas.urjc@gmail.com>
# License: GPLv3 <http://www.gnu.org/licenses/gpl-3.0.html>
#
# https://gitlab.com/varribas-pfm/pose2file/blob/master/scripts/fps_listener.py
# https://github.com/uzh-rpg/rpg_svo/blob/3697e5568dcf1f8729a21a029680fa25522918f9/rqt_svo/src/rqt_svo/svo_widget.py

import rospy
from std_msgs.msg import Float32 as rosFloat
from svo_msgs.msg import Info as svoInfo


def callback(data):
        #rospy.loginfo("fps: %s", data)
	secs = data.processing_time
	if secs > 0:
		fps = 1.0/secs

		float_msg = rosFloat()
		float_msg.data = fps

		global pub
		pub.publish(float_msg)
	

pub = None
if __name__ == '__main__':
        rospy.init_node('svo_info2fps', anonymous=True)

        in_topic = rospy.get_param('in_topic', '/svo/info')
        out_topic = rospy.get_param('out_topic', '/svo/fps')

        pub = rospy.Publisher(out_topic, rosFloat, queue_size=50)
        rospy.Subscriber(in_topic, svoInfo, callback)
        rospy.spin()
