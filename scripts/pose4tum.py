#!/usr/bin/env python
#
# Copyright (c) 2016
# Author: Victor Arribas <v.arribas.urjc@gmail.com>
# License: GPLv3 <http://www.gnu.org/licenses/gpl-3.0.html>


import sys
import rospy
from geometry_msgs.msg import PoseStamped as rosPose


def callback(data):
	#rospy.loginfo("pose: %s", data)
	stamp = data.header.stamp
	pos = data.pose.position
	rot = data.pose.orientation
	line = "%.6f %.4f %.4f %.4f %.4f %.4f %.4f %.4f\n" %(stamp.to_sec(), pos.x, pos.y, pos.z, rot.x, rot.y, rot.z, rot.w)
	#print line,
	global f
	f.write(line)
	

f = object()
if __name__ == '__main__':
        rospy.init_node('pose4tum', anonymous=True)

        filepath = rospy.get_param('~file', 'pose.tum')
        topic = rospy.get_param('~topic', '/lsd_slam/pose')

        with open(filepath, 'w') as f:
            rospy.Subscriber(topic, rosPose, callback)
            rospy.spin()
