#!/usr/bin/env python
#
# Copyright (c) 2016
# Author: Victor Arribas <v.arribas.urjc@gmail.com>
# License: GPLv3 <http://www.gnu.org/licenses/gpl-3.0.html>


import sys
import rospy
from std_msgs.msg import Float32 as rosFloat
# ToDo: move to a Float32/64 stamped message or MultiArray

def callback(data):
        #rospy.loginfo("fps: %s", data)
	global f
	f.write('%.6e\n' % data.data)
	

f = None
if __name__ == '__main__':
        rospy.init_node('fps_listener2', anonymous=True)

        filepath = rospy.get_param('file', 'fps.txt')
        topic = rospy.get_param('topic', '/fps')

	with open(filepath, 'w') as f:
	        rospy.Subscriber(topic, rosFloat, callback)
        	rospy.spin()

		f.close()
		print "fps_listener2 ended."
