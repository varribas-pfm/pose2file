#!/usr/bin/env python
#
# Copyright (c) 2016
# Author: Victor Arribas <v.arribas.urjc@gmail.com>
# License: GPLv3 <http://www.gnu.org/licenses/gpl-3.0.html>


import sys
import rospy
from std_msgs.msg import Float32 as rosFloat

import numpy as np


vfps = []


def callback(data):
        #rospy.loginfo("fps: %s", data)
	vfps.append(data.data)
	

if __name__ == '__main__':
        rospy.init_node('fps_listener', anonymous=True)

        filepath = rospy.get_param('~file', 'fps.txt')
        topic = rospy.get_param('~topic', '/lsd_slam/fps')

        rospy.Subscriber(topic, rosFloat, callback)
        rospy.spin()

        if len(vfps) == 0:
            print "Nothing to do."
            sys.exit(1)

        print 'Evaluating fps...'
        vfps = np.array(vfps)
        fps_mean = np.mean(vfps)
        fps_median, fps_p95 = np.percentile(vfps, (50, 100-95) ) # reverse due 1/x

        print "  mean: %.3f\n  median: %.3f\n  p95: %.3f\n" %(fps_mean, fps_median, fps_p95)
        with open(filepath, 'w') as f:
                formatted = "%.6f %.6f %.6f\n" %(fps_mean, fps_median, fps_p95)
                f.write(formatted)
