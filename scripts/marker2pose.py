#!/usr/bin/env python
#
# Copyright (c) 2016
# Author: Victor Arribas <v.arribas.urjc@gmail.com>
# License: GPLv3 <http://www.gnu.org/licenses/gpl-3.0.html>


import rospy
from geometry_msgs.msg import PoseStamped as rosPose
from visualization_msgs.msg import Marker as rosMarker


def callback(data):
	#rospy.loginfo("pose: %s", data)
	pose_msg = rosPose()
	pose_msg.header = data.header
	pose_msg.pose = data.pose

	global pub
	pub.publish(pose_msg)
	

pub = None
if __name__ == '__main__':
	rospy.init_node('marker2pose', anonymous=True)

	in_topic  = rospy.get_param('~in_topic', '/markers')
	out_topic = rospy.get_param('~out_topic', '/points')

	pub = rospy.Publisher(out_topic, rosPose, queue_size=50)
        rospy.Subscriber(in_topic, rosMarker, callback)
        rospy.spin()
