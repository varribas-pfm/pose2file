#!/usr/bin/env python
#
# Copyright (c) 2016
# Author: Victor Arribas <v.arribas.urjc@gmail.com>
# License: GPLv3 <http://www.gnu.org/licenses/gpl-3.0.html>


import rospy
from geometry_msgs.msg import PoseStamped as rosPose
from geometry_msgs.msg import PoseWithCovarianceStamped as rosPoseCov


def callback(data):
	#rospy.loginfo("posecov: %s", data)
	pose_msg = rosPose()
	pose_msg.header = data.header
	pose_msg.pose = data.pose.pose

	global pub
	pub.publish(pose_msg)
	

pub = None
if __name__ == '__main__':
	rospy.init_node('posecov2pose', anonymous=True)

	in_topic  = rospy.get_param('~in_topic', '/points_in')
	out_topic = rospy.get_param('~out_topic', '/points_out')

	pub = rospy.Publisher(out_topic, rosPose, queue_size=50)
        rospy.Subscriber(in_topic, rosPoseCov, callback)
        rospy.spin()
